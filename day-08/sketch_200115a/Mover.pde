public class Mover {
  public PVector position = new PVector();
  private PVector velocity = new PVector();
  private PVector acceleration = new PVector();
  public float scale = 30;
  private boolean isBreaking = false;
  
  public void render() {
    update(); 
    
    circle(position.x,position.y,scale);  
  }
  
  private void update() {
    position.add(velocity);
  }
  
  public void accelerate(PVector acc, float limit) {   
    isBreaking = false;
    acceleration = acc;
    velocity.add(acceleration);
    velocity.limit(limit);
  }
  
  public void brake() {     
    if (!isBreaking) acceleration.mult(-1);
    isBreaking = true;
    
    //check if adding both results in going backwards
    if (velocity.x + acceleration.x <= 0) {
      isBreaking = false;
      velocity.mult(0);
      acceleration.mult(0);
    };
    velocity.add(acceleration);

  }
}
