Mover mover;
void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ,
        0, 0, 0,
        0, -1, 0);
  mover = new Mover();
  mover.position.set(Window.left, 0);
}

int frame = 0;

void draw() {
  background(150);
  mover.render();
  
  if (mover.position.x >= 0) 
    mover.brake();
  else 
    mover.accelerate(new PVector(0.1, 0), 13);
    
    
  frame++;
  if (frame > 300){
    mover.position.set(Window.left, 0);
    frame = 0;
  }
}
