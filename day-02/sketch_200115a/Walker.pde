class Walker {
  
  float xPosition;
  float yPosition;
  color c = color(50, 50, 50);
  
  Walker() {
  }
  
  Walker(float x, float y) {
    xPosition = x;
    yPosition = y;
  }
  
  void walk(float x, float y) {
    xPosition += x; yPosition += y;
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render() {
    fill(c);
    circle(xPosition, yPosition, 30);
  }
}
