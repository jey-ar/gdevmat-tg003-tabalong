void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0 / 180.0),
        0, 0, 0,
        0, -1, 0);
  background(130);
}

Walker walker = new Walker();

void draw() {
  walker.render();
  
  int moveUp = floor(random(2)); // 0 -> 1
  if (moveUp == 0) { walker.walk(0, 10); }
  else {
    int direction = floor(random(7)); // 0 -> 7
    if (direction == 0) walker.walk(0, -10);
    if (direction == 1) walker.walk(10, 0);
    if (direction == 2) walker.walk(-10, 0);
    if (direction == 3) walker.walk(10, 10);
    if (direction == 4) walker.walk(10, -10);
    if (direction == 5) walker.walk(-10, 10);
    if (direction == 6) walker.walk(-10, -10);
  }
  
  color newColor = color(floor(random(255)), floor(random(255)), floor(random(255)), 50);
  walker.changeColor(newColor);
}
