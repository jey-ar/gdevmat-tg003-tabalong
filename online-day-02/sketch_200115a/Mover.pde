public class Mover {
  private PVector mPosition = new PVector();  
  private PVector mVelocity = new PVector(); 
  private PVector mAcceleration = new PVector();
  private float mScale = 10, mMass = 1;  
  private color c = color(50, 50, 50);
   
  Mover(PVector position, float scale, float mass) {
    mPosition = position;
    mScale = scale;
    mMass = mass;
  }
    
  public void Update() {  
    this.mVelocity.add(this.mAcceleration);
    this.mVelocity.limit(10);
    
    if (this.mPosition.y < Window.bottom) {
      mPosition.y = Window.bottom;
      BounceY();
    }    
    if (this.mPosition.x > Window.right) {
      mPosition.x = Window.right;
      BounceX();
    }
   
    this.mPosition.add(mVelocity);
    this.mAcceleration.mult(0);
  }
      
  public void Render() {
    fill(c);
    circle(mPosition.x,mPosition.y,mScale);  
  }
  
  public void ApplyForce(PVector force) {
    PVector newForce = PVector.div(force, this.mMass);
    this.mAcceleration.add(newForce);
  }
  
  public void SetColor(color newColor) {
    c = newColor;
  }
     
  public void BounceY() {
    this.mVelocity.y *= -1;
  }
 
  public void BounceX() {
    this.mVelocity.x *= -1;
  }
}
