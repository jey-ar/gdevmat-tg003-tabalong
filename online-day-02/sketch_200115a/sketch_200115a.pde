ArrayList<Mover> balls = new ArrayList<Mover>();
PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1);
PVector naturalForce = PVector.add(wind, gravity);

void setup() {
  //1920, 1080
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(255);
  for (int i = 0; i < 10; i++){
    Mover newBall = new Mover(new PVector(Window.left / 2, Window.top / 2), 20 + (i * 5) , 1 + i);     
    color newColor = color(random(10, 240) ,random(10, 240), random(10, 240), 75); 
    newBall.SetColor(newColor);    
    balls.add(newBall);
  }  
}

void draw() {
  background(255);  
  for (Mover ball : balls) {   
    ball.ApplyForce(naturalForce);    
    ball.Update();
    ball.Render();
  }
}
