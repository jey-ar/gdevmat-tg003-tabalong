//QUIZ 2 - PERLIN WALK
float r_t, g_t, b_t;
float x_t, y_t;
float s_t;
int frame;
void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0 / 180.0),
        0, 0, 0,
        0, -1, 0);
  background(0);
  r_t = 200; g_t = 175; b_t = 150;
  x_t = 6; y_t = 3;
  s_t = 200;
  frame = 500;
  frameRate(30);
}

Walker walker = new Walker();
void draw() {
  //Frame Reset
  if (frame >= 500) {
    frame = 0;
    background(0);
  } else { frame++; }
  
  //Move
  x_t += 0.01f; y_t += 0.01f;
  walker.move(noise(x_t) * width - (width/2), noise(y_t) * height - (height/2));
  
  //Color
  r_t += 0.1f; g_t += 0.1f; b_t += 0.1f;
  color newColor = color(noise(r_t) * 200, noise(g_t) * 175, noise(b_t) * 150, 100); 
  walker.changeColor(newColor);
  
  //Size and Render
  s_t += 0.01f;
  walker.render(noise(s_t) * 400);  
}
