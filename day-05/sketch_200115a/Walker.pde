class Walker {
  
  float xPosition, yPosition;
  color c = color(50, 50, 50);
  
  Walker() {
  }
  
  Walker(float x, float y) {
    xPosition = x;
    yPosition = y;
  }
    
  void move(float x, float y) {
    xPosition = x; yPosition = y;
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render(float size) {
    fill(c);
    noStroke();
    circle(xPosition, yPosition, size);
  }
}
