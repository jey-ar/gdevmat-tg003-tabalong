float move, amp, freq;

void setup() {
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan (PI * 30/ 180), 
    0, 0, 0, 
    0, -1, 0);
  move = 0; amp = 30; freq = 0.5;
}

void draw() {
  background(255);
  if (keyPressed) {
    if (key == 'w' || key == 'W') amp++;
    if (key == 's' || key == 'S') amp--;
    if (key == 'd' || key == 'D') freq+=0.01;
    if (key == 'a' || key == 'A') freq-=0.01;
  }
  drawCartesianPlane(200, 100);
  drawSine();
  //drawLinearFunction();
  //drawQuadraticFunction();
  //drawCircle(50);
}

void drawSine() {
  float a = 0, inc = TWO_PI/100;
  for (float x = -width; x < width; x += 1 ) {
    circle(x + move, amp * (float)Math.sin(a * freq), 1);
    a = a + inc;
  }
  move++;
  if (move > width/2) move = 0;
}

void drawCircle(int radius) {
  for (int x = 0; x < 360; x++)
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
}

void drawQuadraticFunction() {
  for (float x = -300; x <= 300; x += 0.1)
    circle(x * 10, (x * x) + (2 * x) - 5, 1);
}

void drawLinearFunction() {
  for (int x = -200; x <= 200; x++)
    circle(x, x + 2, 1);
}

void drawCartesianPlane(int xSize, int ySize) {
  xSize *= 2; ySize *= 2;
  line(0, -ySize, 0, ySize);
  line(xSize, 0, -xSize, 0);
  for (int i = -xSize; i <= xSize; i+=10) 
    line(i, -5, i, 5);
  for (int i = -ySize; i <= ySize; i+= 10) 
    line(-5, i, 5, i);
}
