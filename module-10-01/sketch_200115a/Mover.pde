public class Mover {
  public PVector mPosition = new PVector();  
  public PVector mVelocity = new PVector(); 
  private PVector mAcceleration = new PVector();
  private float mScale = 10;  
  private color c = color(50, 50, 50);
  public float mMass = 1;
  
  Mover(float xPos, float yPos) {
    mPosition.set(xPos, yPos);
  }
   
  Mover(PVector position, float scale, float mass) {
    mPosition = position;
    mScale = scale;
    mMass = mass;
  }
    
  public void Update() {  
    this.mVelocity.add(this.mAcceleration);
    this.mVelocity.limit(10);
           
    this.mPosition.add(mVelocity);
    this.mAcceleration.mult(0);
  }
      
  public void Render() {
    fill(c);
    circle(mPosition.x,mPosition.y,mScale);  
  }
  
  public void ApplyForce(PVector force) {
    PVector newForce = PVector.div(force, this.mMass); 
    this.mAcceleration.add(newForce);
  }
  
  //2.1. Real world Gravity
  public void ApplyGravity(float gravity) {
    PVector grav = new PVector(0, gravity * this.mMass);
    this.ApplyForce(grav);
  }
  
  //2.2. Friction
  public void ApplyFriction(float frictionCoefficient) {
    float norm = 1;
    float fMag = frictionCoefficient * norm;
    PVector fric = this.GetVelocity();
        
    fric.mult(-1);
    fric.normalize();
    fric.mult(fMag);
        
    this.ApplyForce(fric);
  }
  
  public PVector GetVelocity() {
    PVector velocity = new PVector(mVelocity.x, mVelocity.y);
    if (velocity.mag() < 0.01) return velocity.mult(0);
    return velocity;
  }
    
  public void SetColor(color newColor) {
    c = newColor;
  }
}
