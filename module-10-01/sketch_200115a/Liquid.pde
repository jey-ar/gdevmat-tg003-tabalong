public class Liquid {
  public float x, y;
  public float hor, dep;
  public float cd;
  
  Liquid(float xPos, float yPos, float horizon, float depth, float coeffDrag) {
    x = xPos;
    y = yPos;
    hor = horizon;
    dep = depth;
    cd = coeffDrag;
  }

  public void Render() {
    fill(28, 120, 186);
    beginShape();
    vertex(x - hor, y, 0);
    vertex(x + hor, y, 0);
    vertex(x + hor, y + dep, 0);
    vertex(x - hor, y + dep, 0);
    endShape();
  }
  
  public boolean IsCollidingWith(Mover mover) {
    PVector pos =  mover.mPosition;
    return pos.x >= this.x - this.hor &&
      pos.x <= this.x + this.hor &&
      pos.y <= this.y;
  }
  
  public PVector CalculateDragForce(Mover mover) {
    float speed = mover.mVelocity.mag();
    float dragMag = cd * speed * speed;
    
    PVector dragForce = mover.mVelocity.copy();
    dragForce.mult(-1);
    
    dragForce.normalize();
    dragForce.mult(dragMag);
    
    return dragForce;
  }
  
}
