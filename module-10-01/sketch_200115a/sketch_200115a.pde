ArrayList<Mover> beachBalls = new ArrayList<Mover>();
PVector windForce = new PVector(2, 0);
float gravityForce =  -0.1f;
int frames = 0;
Liquid ocean = new Liquid(0, 0, Window.right, Window.bottom, 1f);

void setup() {
  size(1080, 720, P3D); //1920, 1080
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(255); 
}

void draw() {
  background(255);  
  
  if (frames == 0) beachBalls = DrawBalls(10);
  
  ocean.Render();
  noStroke();
  
  for (Mover ball : beachBalls) {
    ball.Render();
    ball.Update();
    ball.ApplyGravity(gravityForce);
  
    //2.3-4. Drag and Wind Force
    if (ocean.IsCollidingWith(ball)) 
      ball.ApplyForce(ocean.CalculateDragForce(ball));
    else
      ball.ApplyForce(windForce);
     
    //2.5. Newton bounce
    if (ball.mPosition.y < Window.bottom) {
      ball.mPosition.y = Window.bottom;
      ball.mVelocity.y *= -1;
    }
    
    if (ball.mPosition.x > Window.right) {
      ball.mPosition.x = Window.right;
      ball.mVelocity.x *= -1;
    }
  }
  
  if (frames >= 800) frames = 0;
  else frames++;
}

//1. Mover Initialization
ArrayList<Mover> DrawBalls(int amount){
  ArrayList<Mover> newBalls = new ArrayList<Mover>();
  float rightOffset = 20;
  
  for (int i = 0; i < amount; i++){
    float xPosition = Window.right - (i * Window.widthPx / amount) - rightOffset;
    float massSize = 20 + (i * 5);
    Mover newBall = new Mover(new PVector(xPosition, Window.top), massSize, massSize);     
    color newColor = color(random(10, 240) ,random(10, 240), random(10, 240), 100); 
    newBall.SetColor(newColor);    
    newBalls.add(newBall);
  }  
  
  return newBalls;
}
