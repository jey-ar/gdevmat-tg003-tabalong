void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(255);
  
  r_t = 200; g_t = 175; b_t = 150;
}

Vector2 mousePos() {
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

float saberLength = 700;
float handleLength = 70;
float rot_t = 50;
float r_t, g_t, b_t;

void draw() {
  background(255);
  
  Vector2 mouse = mousePos();
  Vector2 handle = mousePos();
  
  mouse.normalize();
  mouse.mult(saberLength);
  
  handle.normalize();
  handle.mult(handleLength);
  
  rot_t += 0.01f;
  rotate(radians(noise(rot_t) * 360));
  
  
  //Glow
  strokeWeight(50);
  r_t += 0.1f; g_t += 0.1f; b_t += 0.1f;
  stroke(noise(r_t) * 200, noise(g_t) * 175, noise(b_t) * 150, 50);
  line(handle.x, handle.y, mouse.x, mouse.y);
  line(-handle.x, -handle.y, -mouse.x, -mouse.y);
  
  //Saber
  strokeWeight(20);
  stroke(noise(r_t) * 200, noise(g_t) * 175, noise(b_t) * 150);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  //Handle
  strokeWeight(30);
  stroke(0, 250, 250);
  line(0, 0, handle.x, handle.y); //Main
  line(0, 0, -handle.x, -handle.y); //Main
}
