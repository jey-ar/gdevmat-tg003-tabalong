class Walker {
  
  Vector2 position;
  color c = color(50, 50, 50);
  float scale;
  
  Walker() {
    position = new Vector2();
    scale = 30;
  }
  
  Walker(float x, float y, float s) {
    position = new Vector2(x, y);
    scale = s;
  }
  
  void walk(float x, float y) {
    this.position.x += x; this.position.y += y;
  }
  
  void walkRandomly() {
    int direction = floor(random(8)); // 0 -> 7
    if (direction == 0) this.walk(0, -10);
    if (direction == 1) this.walk(10, 0);
    if (direction == 2) this.walk(-10, 0);
    if (direction == 3) this.walk(10, 10);
    if (direction == 4) this.walk(10, -10);
    if (direction == 5) this.walk(-10, 10);
    if (direction == 6) this.walk(-10, -10);
    if (direction == 7) this.walk(0, 10);
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render() {
    fill(c);
    circle(position.x, position.y, scale);
  }
}
