ArrayList<Mover> balls = new ArrayList<Mover>();
PVector acceleration = new PVector(0.2f, 0);
int frames = 0;

void setup() {
  size(1080, 720, P3D); //1920, 1080
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(255); 
}

void draw() {
  background(255);  
  if (frames==0) balls = DrawBalls(5);
   
  for (Mover ball : balls) {      
    if (ball.mPosition.x < 0) 
      ball.ApplyForce(acceleration);    
    else 
      ball.ApplyFriction(0.3f);
      
    ball.Update();
    ball.Render();   
  }
  
  if (frames >= 800) frames = 0;
  else frames++;
}

ArrayList<Mover> DrawBalls(int amount){
  ArrayList<Mover> newBalls = new ArrayList<Mover>();
  for (int i = 0; i < amount; i++){
    float randomFloat = random(5, 30);
    Mover newBall = new Mover(new PVector(Window.left, Window.top - (i * Window.heightPx / amount)), 20 + randomFloat , 1 + randomFloat);     
    color newColor = color(random(10, 240) ,random(10, 240), random(10, 240), 75); 
    newBall.SetColor(newColor);    
    newBalls.add(newBall);
  }  
  return newBalls;
}
