class Ball {
  
  float xPosition, yPosition;
  color c = color(50, 50, 50);
  
  
  Ball() {
  }
  
  Ball(float x, float y) {
    xPosition = x;
    yPosition = y;
  }
    
  void move(float x, float y) {
    xPosition += x; yPosition += y;
  }
  
  float getXPos(){
    return xPosition;
  }
  
  float getYPos(){
    return yPosition;
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render(float size) {
    fill(c);
    noStroke();
    circle(xPosition, yPosition, size);
  }
}
