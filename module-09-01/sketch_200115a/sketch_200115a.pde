ArrayList<Mover> balls = new ArrayList<Mover>();
PVector wind = new PVector(0.1f, 0);
float gravity = -0.5f;

void setup() {
  //1920, 1080, 1080, 720
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(255);
  
  for (int i = 0; i < 10; i++){
    Mover newBall = new Mover(new PVector(Window.left + (i * 100) + 10, 0), 20 + (i * 5) , 1 + (i * 5));     
    color newColor = color(random(10, 240) ,random(10, 240), random(10, 240), 75); 
    newBall.SetColor(newColor);    
    balls.add(newBall);
  }  
  
}

void draw() {
  background(255);  
  
  for (Mover ball : balls) {   
    ball.ApplyForce(wind);  
    ball.ApplyGravity(gravity);  
    ball.Update();
    ball.Render();
  }
  
}
