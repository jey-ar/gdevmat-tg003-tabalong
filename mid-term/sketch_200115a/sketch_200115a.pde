//Midterms Blackholes
float countDown = 0, resetTime = 2, lastFrame = 0, dt = 0;
void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(0);
}

ArrayList<Ball> stars = new ArrayList<Ball>();
Ball blackHole = new Ball(0, 0, 50);

void draw() {
  //get detlaTime
  dt = (millis() - lastFrame) / 1000; 
  background(0); 
  if (countDown <= 0) {
    
    //Use random for all most but spawn position of stars
    PVector newRanPos = new PVector(random(Window.left,Window.right),random(Window.bottom,Window.top));
    blackHole.pos.set(newRanPos);
    
    for (int i = stars.size() - 1; i >= 0; i--) stars.remove(i);
    
    float starAmount = random(100, 100 + random(100)); 
    for (int i = 0; i < floor(starAmount); i++){
      PVector newPosGaus = new PVector(GenerateGaussian(Window.right/2,0),GenerateGaussian(Window.top/2,0));
      Ball newStar = new Ball(newPosGaus, random(10, 30));
      color ranColor = color(random(255), random(255), random(255), random(255));
      newStar.changeColor(ranColor);
      stars.add(newStar);
    }  
    countDown = resetTime;
  } else { countDown -= 1 * dt; }

  
  for (Ball star : stars) {
    star.moveTowards(blackHole.pos, 450 * dt);
    star.render();
  }
  
  blackHole.render();
  lastFrame = millis();
}

float GenerateGaussian(float sd, float mean) {
  return (randomGaussian() * sd) + mean;
}

float GenerateNoise(float seed, float scale, float offset) {
  return (noise(seed) * scale) + offset;
}
