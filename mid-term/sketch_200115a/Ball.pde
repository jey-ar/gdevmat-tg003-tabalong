class Ball {  
  PVector pos = new PVector(0, 0);
  float s = 10;
  color c = color(255, 255, 255);
  
  Ball(float x, float y, float scale) {
    pos.set(x,y);
    s = scale;
  }
  
  Ball(PVector vecPosition, float scale) {
    pos.set(vecPosition);
    s = scale;
  }
    
  void moveTowards(PVector vecPos, float speed){    
    PVector direction = PVector.sub(vecPos, pos);       
    direction.normalize();
    direction.mult(speed);  
    pos.add(direction);
  }
          
  void changeSize(float scale) {
    s = scale;
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render() {
    fill(c);
    noStroke();
    circle(pos.x, pos.y, s);
  }
}
