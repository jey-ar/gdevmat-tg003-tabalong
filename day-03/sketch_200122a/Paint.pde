class Paint {
  
  float xPosition;
  float yPosition;
  float size;
  color c = color(50, 50, 50);
  
  Paint() {
  }
  
  Paint(float x, float y) {
    xPosition = x;
    yPosition = y;
  }
  
  void splat(float x, float y, float s) {
    xPosition = x; yPosition = y;
    size = s;
  }
  
  void changeColor(color newColor) {
    c = newColor;
  }
  
  void render() {
    fill(c);
    if (size <= 0) size = 1; 
    noStroke();
    circle(xPosition, yPosition, size);
  }
}
