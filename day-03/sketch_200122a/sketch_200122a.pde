int frame;

void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height/2.0) / tan(PI * 30.0 / 180.0),
        0, 0, 0,
        0, -1, 0);
  background(255);
  frame = 100;
  frameRate(30);
}

Paint paintSplatter = new Paint();

void draw() {   
  if (frame >= 100) {    
    float volume = GenerateGaussian(30, 60);
    background(255);
    frame = 0;
    
    for (int i = 0; i < floor(volume); i++) {
      float xPos = GenerateGaussian(400, width/2); 
      float yPos = GenerateGaussian(200, height/2); 
      xPos -= width/2; yPos -= height/2; //Offsetting
      float size = GenerateGaussian(40, 70); 
      paintSplatter.splat(xPos, yPos, size);
    
      color newColor = color(random(255), random(255), random(255), 50);
      paintSplatter.changeColor(newColor);
      paintSplatter.render();   
    }
  }
  
  frame++;
  println(frame);
}

float GenerateGaussian(float sd, float mean) {
    float random = randomGaussian();
    return (random * sd) + mean;
}
